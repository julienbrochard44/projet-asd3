import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.ConcurrentSkipListMap;

public class MultiMap {
	private ConcurrentSkipListMap <Double, Deque<QuadTree>> mapList;
	
	public MultiMap() {
		this.mapList = new ConcurrentSkipListMap <Double, Deque<QuadTree>>();
	}
	
	// insert a new key or insert in the Deque of an existing key
	public void insert(Double key, QuadTree tree) {
		if(this.mapList.containsKey(key)) {
			this.mapList.get(key).addLast(tree);
		}else {
			Deque<QuadTree> list = new ArrayDeque<QuadTree>();
			list.addLast(tree);
			this.mapList.put(key, list);
		}
	}
	
	// not used, get and remove from a specified key
	public QuadTree getAndRemove(Double key) {
		if(this.mapList.size() > 0) {
			if(this.mapList.get(key).size() > 1) {
				QuadTree out = this.mapList.get(key).getFirst();
				this.mapList.get(key).removeFirst();
				return out;
			} else if (this.mapList.firstEntry().getValue().size() == 1) {
				QuadTree out = this.mapList.get(key).getFirst();
				this.mapList.remove(key);
				return out;
			}
		}
		System.out.println("MultiMap vide");
		return null;
	}
	
	// get and remove the first element of the first key
	public QuadTree getAndRemoveFirst() {
		if(this.mapList.size() > 0) {
			if(this.mapList.firstEntry().getValue().size() > 1) {
				QuadTree out = this.mapList.firstEntry().getValue().getFirst();
				this.mapList.firstEntry().getValue().removeFirst();
				return out;
			} else if (this.mapList.firstEntry().getValue().size() == 1) {
				QuadTree out = this.mapList.firstEntry().getValue().getFirst();
				this.mapList.remove(this.mapList.firstKey());
				return out;
			}
		}
		System.out.println("MultiMap vide");
		return null;
	}
	
	// get the number of elements
	public Integer getSize() {
		return this.mapList.size();
	}
	
}
