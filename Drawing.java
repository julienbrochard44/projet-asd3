import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.Color;

// not used but useful to show a quadtree without converting it to png
public class Drawing extends JPanel{

	public Color[][] pixels;
	public BufferedImage img;
	
	public void paint(Graphics g){
		super.paint(g);

	}

	public Drawing(Color[][] pixels){
		this.pixels = pixels;
		this.img = null;
		this.setSize(pixels[0].length, pixels.length);
		this.setBounds(pixels[0].length, pixels.length, 0, 0);
		//this.setBorder(BorderFactory.createLineBorder(Color.red));
		this.setVisible(true);
	}
	
	public Drawing(ImagePNG img){
		this.pixels = null;
		this.img = img.getBufferedImage();
		this.setSize(img.width(), img.height());
		this.setBounds(img.width(), img.height(), 0, 0);
		this.setVisible(true);
	}
	
	@Override
	public void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    if(this.pixels != null) {
		    for(int y = 0; y < this.pixels.length; y++) {
				for(int x = 0; x < this.pixels[y].length; x++) {
					if(pixels[y][x] != null) {
						g.setColor(this.pixels[y][x]);
						g.drawRect(x, y, 1, 1);
					}
				}
			}
	    }
	    else {
	    	g.drawImage(this.img, 0, 0, null);
	    }
	}
}