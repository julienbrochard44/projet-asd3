import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class QuadTree {
	
	//*****************************************
	//             ATTRIBUTES
	//*****************************************
	
	public QuadTree tl; // top left
	public QuadTree tr; // top right
	public QuadTree br; // bottom right
	public QuadTree bl; // bottom left
	public Color color;
	
	//*****************************************
	//             CONSTRUCTEURS
	//*****************************************
	
	/* 
	 * Role : Constructor of an Empty QuadTree
	 */
	public QuadTree(){
		this.tl = null;
        this.tr = null;
        this.br = null;
        this.bl = null;
        this.color = null;
	}
	
	/* 
	 * Role : Constructor of a QuadTree from an ImagePNG
	 * Precondition : The ImagePNG has to be a square (width = height)
	 */
	public QuadTree(ImagePNG img) throws IOException {
        this.tl = null;
        this.tr = null;
        this.br = null;
        this.bl = null;
        this.color = null;

        this.createTreeWithImage(img, 0, 0, img.width());
        //System.out.println("Affichage en string du QuadTree de l image : " + this.toString());
        this.compressDelta(0);
        //System.out.println("Affichage en string du QuadTree de l image avec compression sans degradation : " + this.toString());
	}
	
	//*****************************************
	//		CONSTRUCTOR CREATOR
	//*****************************************
	
	/*  
	 * Role : Create a QuadTree from an ImagePNG
	 */
	public void createTreeWithImage(ImagePNG img, int x, int y, int w) {
		if (w == 1) {
            this.color = img.getPixel(x, y);
        } else {
        	this.tl = new QuadTree();
            this.tl.createTreeWithImage(img, x, y, w/2);
            this.tr = new QuadTree();
            this.tr.createTreeWithImage(img, x + w/2, y, w/2);
            this.bl = new QuadTree();
            this.bl.createTreeWithImage(img, x, y + w/2, w/2);
            this.br = new QuadTree();
            this.br.createTreeWithImage(img, x + w/2, y + w/2, w/2);
        }
	}
	
	
	
	//*****************************************
	//		COMPRESS DELTA METHODS
	//*****************************************
	
	/*
	 * Role : Apply a delta compression to this QuadTree (deletes every leaf with a Color gap underneath delta)
	 */
	public boolean compressDelta(int delta) {
		if(delta < 0 || delta > 192) throw new IllegalArgumentException("Delta >= 0 && Delta <= 192");
		
		if(!this.isALeaf() && this.hasFourLeaf()) {
			if(this.getBiggestColorGap() <= delta) {
				// Calculate and store Average
				this.color = this.average();
				this.tl = null;
		        this.tr = null;
		        this.br = null;
		        this.bl = null;
		        return true;
			}
		}
		else {
			// Parcours les branches
			boolean retry = false;
			if(this.tl != null && !this.tl.isALeaf()) {
				if(this.tl.compressDelta(delta)) {
					retry = true;
				}
			}
			if(this.tr != null && !this.tr.isALeaf()) {
				if(this.tr.compressDelta(delta)) {
					retry = true;
				}
			}
			if(this.bl != null && !this.bl.isALeaf()) {
				if(this.bl.compressDelta(delta)) {
					retry = true;
				}
			}
			if(this.br != null && !this.br.isALeaf()) {
				if(this.br.compressDelta(delta)) {
					retry = true;
				}
			}
			if(retry) {
				return this.compressDelta(delta);
			}
		}
		return false;
	}
	
	//*****************************************
	//		COMPRESS PHI METHODS
	//*****************************************

    /*
     * Role : Apply a phi compression to this QuadTree (deletes leaves with the smallest Color gap  until there is a phi or less number of leaves)
     */
    public void compressPhi(int phi) {
    	if(phi < 1 || phi > this.getNumberOfLeaf()) throw new IllegalArgumentException("Phi >= 1 && Phi <= Number of leaf");

        MultiMap colorGapList = new MultiMap();
        HashMap<QuadTree,QuadTree> parent = new HashMap<QuadTree, QuadTree>();
        this.createMaps(colorGapList, parent); // Fill colorGapList and parent

        int nbFeuilles = this.getNumberOfLeaf();
        if(phi < 1 && phi > this.getNumberOfLeaf()) throw new IllegalArgumentException("Phi >= 1 && Phi <= Number of leaf");
        while(nbFeuilles > phi) {
            // Get the QuadTree with the lowest Color gap
            QuadTree tree = colorGapList.getAndRemoveFirst();
            QuadTree father = parent.get(tree);
            parent.remove(tree);
            tree.color = tree.average();
            tree.tl = null;
            tree.tr = null;
            tree.br = null;
            tree.bl = null;
            nbFeuilles -= 3;
            // Try to insert the QuadTree if nbFeuilles != 1 in case the root is a leaf
        	if(nbFeuilles != 1) {
        		father.addIfHasFourLeaf(colorGapList);
       		}
        }
    }
    
    /*
     * Role : Add this QuadTree in the MultiMap if it has four leaves
     */
    public void addIfHasFourLeaf(MultiMap colorGapList) {
        if(this.hasFourLeaf()) {          
            colorGapList.insert(this.getBiggestColorGap(), this);
        }
    }
    
    /*  
     * Role : Fills colorGapList (with the ColorGap and the QuadTree corresponding) and parents (with the child and its parent)
     */
    public  void createMaps(MultiMap colorGapList, HashMap<QuadTree, QuadTree> parent) {
        if(this.hasFourLeaf()) {          
            colorGapList.insert(this.getBiggestColorGap(), this);
        }else {
            if(!this.tl.isALeaf()){
                parent.put(this.tl, this);
                this.tl.createMaps(colorGapList, parent);
            }
            if(!this.tr.isALeaf()){
                parent.put(this.tr, this);
                this.tr.createMaps(colorGapList, parent);
            }
            if(!this.bl.isALeaf()){
                parent.put(this.bl, this);
                this.bl.createMaps(colorGapList, parent);
            }
            if(!this.br.isALeaf()){
                parent.put(this.br, this);
                this.br.createMaps(colorGapList, parent);
            }
        }
    }
	
	//*****************************************
	//             TO PNG METHODS
	//*****************************************
	
    /*  
	 * Role : Make an ImagePNG from the QuadTree
	 */
	public ImagePNG toPNG() {
		int width = this.width();
		BufferedImage img = new BufferedImage(width, width, BufferedImage.TYPE_INT_RGB);
    	for( int x = 0 ; x < width ; x++ ) {
            for( int y = 0 ; y < width ; y++ ) {
            	img.setRGB(x, y, this.getPixelFromTree(x, y, width).getRGB());
            }
		}
    	return new ImagePNG(img);
	}
	
	/*
	 * Role : Make a 2D Color table representing the image
	 */
	public Color[][] getPixels() {
		int width = this.width();
		Color[][] pixels = new Color[width][width];
		for( int x = 0 ; x < width ; x++ ) {
            for( int y = 0 ; y < width ; y++ ) {
            	pixels[y][x] = this.getPixelFromTree(x, y, width);
            }
		}
		return pixels;
	}
		
	/*
	 * Role : Get the Color that should be in the given position
	 */
	public Color getPixelFromTree(int x, int y, int width) {
		if(this.color == null) {
			if(x < width/2) {
				if(y < width/2) {
					// Cas correspondant au QuadTree en haut a gauche 
					if(this.tl != null) {
						return this.tl.getPixelFromTree(x, y, width/2);
					}
				}
				else {
					// Cas correspondant au QuadTree en bas a gauche 
					if(this.bl != null) {
						return this.bl.getPixelFromTree(x, y-width/2, width/2);
					}
				}
			}
			else {
				if(y < width/2) {
					// Cas correspondant au QuadTree en haut a droite
					if(this.tr != null) {
						return this.tr.getPixelFromTree(x-width/2, y, width/2);
					}
				}
				else {
					// Cas correspondant au QuadTree en bas a droite
					if(this.br != null) {
						return this.br.getPixelFromTree(x-width/2, y-width/2, width/2);
					}
				}
			}
		}
		// je peux pas mettre de else ??
		return this.color;
		
	}
    
	
	//*****************************************
	//		DISPLAY METHODS
	//*****************************************
	
	/*  
	 * Role : Return a panel that could later be displayed
	 */
	public JPanel getPanel(String txt) {
		//JPanel draw = new Drawing(this.getPixels());
		ImagePNG i = this.toPNG();
		JPanel draw = new Drawing(i);
		draw.setPreferredSize(new Dimension(i.width(), i.height()));
		JLabel label = new JLabel("Quadtree");
		label.setBorder(new LineBorder(Color.BLACK));
		JLabel text = new JLabel(txt);
		
		JPanel list = new JPanel();
		//list.setLayout(new BoxLayout(list, BoxLayout.PAGE_AXIS));
		list.add(label);
		list.add(text);
		
		draw.add(list);
    	return draw;
	}
	
	
	/*  
	 * Role : Save as png
	 */
	public void saveImage(String filename) throws IOException {
		this.toPNG().saveImage(filename);
	}
	
	/*  
	 * Role : Save as a text file representing the QuadTree's structure
	 */
	public void saveText(String filename) {
        File f = new File(filename);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(f))){
        	writer.write(this.toString());
        	writer.newLine();
        }catch(IOException e) {
        	e.printStackTrace();
        }
    }

    //*****************************************
  	//		GETTERS AND GLOBAL METHODS
  	//*****************************************
  	
  	/*  
  	 * Role : Return true if this QuadTree is a Leaf (has a Color and no child)
  	 */
      public boolean isALeaf() {
      	return this.color != null;
      }
  	
  	/*
  	 * Role: Return true if this QuadTree has 4 leaves (4 child with a Color and without children)
  	 */
  	public boolean hasFourLeaf() {
  		return this.tl.isALeaf() && this.tr.isALeaf() && this.bl.isALeaf() && this.br.isALeaf();
  	}
  	
  	/*  
  	 * Role : Compute the Color gap (in our case we compare a color with it's parent average)
  	 */
  	public double getColorGap(Color c1, Color moyenne) {
  		return Math.sqrt( (Math.pow(c1.getRed() - moyenne.getRed(), 2) + Math.pow(c1.getGreen() - moyenne.getGreen(), 2) + Math.pow(c1.getBlue() - moyenne.getBlue(), 2) )/ 3);
  	}
  	
  	/*  
  	 * Role : Compute the average Color by averaging the reds, greens and blues of each children
  	 */
  	public Color average() {
  		int r = (this.tl.color.getRed() + this.tr.color.getRed() + this.br.color.getRed() + this.bl.color.getRed())/4;
  		int g = (this.tl.color.getGreen() + this.tr.color.getGreen() + this.br.color.getGreen() + this.bl.color.getGreen())/4;
  		int b = (this.tl.color.getBlue() + this.tr.color.getBlue() + this.br.color.getBlue() + this.bl.color.getBlue())/4;
  		
  		return new Color(r, g, b);
  	}
  	

      
  	/*  
  	 * Role : Convert and return a Color in hex
  	 */
     public String colorToHex(Color col) {
     	return Integer.toHexString(col.getRGB()).substring(2);
     }
      
  	/*  
  	 * Role : Return the QuadTree's structure represented in a String
  	 */
  	public String toString(){
          if(!this.isALeaf() && this.tl != null && this.tl != null && this.bl != null && this.br != null) {
          	return("(" + this.tl.toString() + " " + this.tr.toString() + " "+this.bl.toString() + " "+this.br.toString() + ")");
          }
          else if(this.color != null){
          	return(this.colorToHex(this.color));
          }
          else {
          	return "vide";
          }
	}
  	
  	/*
  	 * Role : Return the width of the image this QuadTree could form
  	 */
  	public int width() {
  		return (int) Math.sqrt(Math.pow(4, this.getSize()));
  	}
  	
  	/*  
  	 * Role : Return this QuadTree's height (the longer branch)
  	 */
  	public int getSize() {
  		if(!this.isALeaf()) {
  			int[] size = {this.tl.getSize(), this.tr.getSize(), this.bl.getSize(), this.br.getSize()};
  			
  			int max = 0;
  			for (int i = 0; i < size.length; i++) {
  	            if (size[i] > max)  {
  	                max = size[i]; 
  	            }
  			}
  			return 1 + max;
  		}
  		return 0;
  	}
      /*  
  	 * Role : Return the number of leaf in this QuadTree
  	 */
      public int getNumberOfLeaf() {
          int counter = 0;

          if (!this.isALeaf()) {
              counter += this.tl.getNumberOfLeaf();
              counter += this.tr.getNumberOfLeaf();
              counter += this.bl.getNumberOfLeaf();
              counter += this.br.getNumberOfLeaf();
          } else {
              counter++;
          }

  	   return counter;
  	}
      
  	/*  
  	 * Role : Compute and return the biggest Color Gap between this QuadTree's children
  	 */
  	public double getBiggestColorGap() {
  		if(!this.isALeaf() && this.hasFourLeaf()) { 
  			
  			// Compute the gaps of the 4 leaves
  			Color moyenne = this.average();
  			double[] gaps = {this.getColorGap(tl.color, moyenne), this.getColorGap(tr.color, moyenne), this.getColorGap(bl.color, moyenne), this.getColorGap(br.color, moyenne)};
  			
  			// Select the biggest gaps of the 4
  			double max = 0;
  			for (int i = 0; i < gaps.length; i++) {
  	            if (gaps[i] > max)  {
  	                max = gaps[i]; 
  	            }
  			}
  			return max;
  		}
  		else {
  			System.out.println("Erreur");
  			return 0;
  		}
  	}
    
}
