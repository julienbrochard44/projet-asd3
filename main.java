import java.io.File;

public class main {
	public static void main( String[] args )
	{
	    try {
	        if (args.length < 1) throw new IllegalArgumentException("Preciser le nom et chemin du fichier");
	
	        ImagePNG png = new ImagePNG(args[0]);
	        File fPng = new File(args[0]);
	        
	        // If there is only an image as argument 
	        if(args.length == 1) {
	        	// Launch the graphical interface
	        	Window window = new Window(args[0]);
	        }
	        // Else launch compress for the given values
	        else if(args.length == 2 || args.length == 3) {
	        	System.out.println("----- Interface console -----");
	        	
	        	// Launch compress for delta
	        	if(Integer.parseInt(args[1]) >= 0 && Integer.parseInt(args[1]) <= 192) {
	        		QuadTree delta = new QuadTree(png);
	        		System.out.println("Compress Delta: " + args[1]);
	        		delta.compressDelta(Integer.parseInt(args[1]));
	        		delta.saveText("compressDelta.txt");
	        		ImagePNG iDelta = delta.toPNG();
	        		iDelta.saveImage("compressDelta.png");
	        		
	        		File fOut = new File("compressDelta.png");
	        		System.out.println("Fin Compress Delta " + delta.getNumberOfLeaf() + " feuilles");
	        		System.out.println("Indice de similarite : " + ImagePNG.computeEQM(png, delta.toPNG()));
	        		System.out.println("Rapport de taille : " + Math.ceil(10000.0*fOut.length() / fPng.length())/100.0);
	        		System.out.println("-----------------------------");
	        	} else {
	        		throw new IllegalArgumentException("Delta [0..192]");
	        	}
	        	if(args.length == 3) {
		        	// Launch compress for phi
		        	if(Integer.parseInt(args[2]) >= 1) {
		        		QuadTree phi = new QuadTree(png);
		        		System.out.println("Compress Phi: " + args[2]);
		        		phi.compressPhi(Integer.parseInt(args[2]));
		        		phi.saveText("compressPhi.txt");
		        		ImagePNG iPhi = phi.toPNG();
		        		iPhi.saveImage("compressPhi.png");
		        		
		        		File fOut = new File("compressPhi.png");
		        		System.out.println("Fin Compress Phi " + phi.getNumberOfLeaf() + " feuilles");
		        		System.out.println("Indice de similarite : " + ImagePNG.computeEQM(png, phi.toPNG()));
		        		System.out.println("Rapport de taille : " + Math.ceil(10000.0 * fOut.length() / fPng.length())/100.0);
		        		System.out.println("-----------------------------");
		        	} else {
		        		throw new IllegalArgumentException("Phi [1..infini]");
		        	}
	        	}
	        } else {
	        	throw new IllegalArgumentException("N'indiquez aucun arguments de plus que l'image pour l'interface graphique ou les arguments delta et phi");
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}