import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Window {
	public Window(String sPNG) throws IOException {

		System.out.println("----- Interface visuelle -----");
		
		final ImagePNG png = new ImagePNG(sPNG);
		final File fPng = new File(sPNG);
        QuadTree test = new QuadTree(png);

        final JFrame frame = new JFrame();
        frame.setLayout(new GridLayout(2, 2));


        
        final GridBagConstraints gbc = new GridBagConstraints();
        
        //*****************************************
    	//             Delta Interface
    	//*****************************************
        final JPanel imagesDelta = new JPanel();
        imagesDelta.setLayout(new GridLayout(0, 1));
        final JScrollPane scrollImagesDelta = new JScrollPane(imagesDelta);
        scrollImagesDelta.getVerticalScrollBar().setUnitIncrement(16);
        
        JPanel interfaceDelta = new JPanel();
		interfaceDelta.setLayout(new GridLayout(2, 2));
		
		
		final JSlider sliderDelta = new JSlider(0, 192);
		
		final JTextArea textAreaDelta = new JTextArea(String.valueOf(sliderDelta.getValue()));
		interfaceDelta.add(textAreaDelta);
		interfaceDelta.add(sliderDelta);
		
		// Action on slider Delta move
		sliderDelta.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
            	textAreaDelta.setText(String.valueOf(sliderDelta.getValue()));
            }
        });
		
		
		
		
		JButton buttonDelta = new JButton("Compress Delta");
		// Action on Button Delta press
		buttonDelta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try  {
					Integer val = Integer.parseInt(textAreaDelta.getText());
					System.out.println("Compress Delta: " + val);
					QuadTree out = new QuadTree(png);
					out.compressDelta(val);
					out.saveText("compressDelta.txt");
					ImagePNG i = out.toPNG();
					i.saveImage("compressDelta.png");
					
					File fOut = new File("compressDelta.png");
					System.out.println("Fin Compress Delta " + out.getNumberOfLeaf() + " feuilles");
                    System.out.println("Indice de similarite Delta : " + ImagePNG.computeEQM(png, i));
                    System.out.println("Rapport de taille Delta : " + Math.ceil(10000.0*fOut.length() / fPng.length())/100.0);
                    
					imagesDelta.add(i.getPanel("Delta " + val + ": " + out.getNumberOfLeaf() + " leaves, " + ImagePNG.computeEQM(png, i) + " similarite, " + Math.ceil(10000.0*fOut.length() / fPng.length())/100.0 + " taille"), gbc);
				} catch (Exception ex) {
					System.out.println(ex);
				}
				
				frame.setVisible(true);
				System.out.println("------------------------------");
			}
		});
		interfaceDelta.add(buttonDelta);
		
		//*****************************************
    	//             Phi Interface
    	//*****************************************
		final JPanel imagesPhi = new JPanel();
        imagesPhi.setLayout(new GridLayout(0, 1));
        final JScrollPane scrollImagesPhi = new JScrollPane(imagesPhi);
        scrollImagesPhi.getVerticalScrollBar().setUnitIncrement(16);
		JPanel interfacePhi = new JPanel();
		interfacePhi.setLayout(new GridLayout(2, 2));
		
		QuadTree out = new QuadTree(png);
		final JSlider sliderPhi = new JSlider(1, out.getNumberOfLeaf());
		final JTextArea textAreaPhi = new JTextArea(String.valueOf(sliderPhi.getValue()));
		//textArea.setText(deltaValue); 
		interfacePhi.add(textAreaPhi);
		interfacePhi.add(sliderPhi);
		
		// Action on slider Phi move
		sliderPhi.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
            	textAreaPhi.setText(String.valueOf(sliderPhi.getValue()));
            }
        });
		
		
		
		JButton buttonPhi = new JButton("Compress Phi");
		// Action on Button Phi press
		buttonPhi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					Integer val = Integer.parseInt(textAreaPhi.getText());
					QuadTree out = new QuadTree(png);
					System.out.println("Compress Phi: " + val);
					
					out.compressPhi(val);
					out.saveText("compressPhi.txt");
					ImagePNG i = out.toPNG();
					i.saveImage("compressPhi.png");
					
					File fOut = new File("compressPhi.png");
					System.out.println("Fin Compress Phi " + out.getNumberOfLeaf() + " feuilles");
					System.out.println("Indice de similarite Phi : " + ImagePNG.computeEQM(png, i));
                    System.out.println("Rapport de taille Phi : " + Math.ceil(10000.0*fOut.length() / fPng.length())/100.0);
                    
					imagesPhi.add(out.getPanel("Phi " + val + ": " + out.getNumberOfLeaf() + " leaves, " + ImagePNG.computeEQM(png, i) + " similarite, " + Math.ceil(10000.0*fOut.length() / fPng.length())/100.0 + " taille"), gbc);
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					System.out.println(ex);
				}
				
				frame.setVisible(true);
				System.out.println("------------------------------");
			}
		});
		interfacePhi.add(buttonPhi);
		
		imagesDelta.add(png.getPanel("PNG de base")); // image
		imagesDelta.add(test.getPanel("Delta 0")); // quadtree
		//imagesDelta.add(test.toPNG().getPanel("Compression delta 40 avec pertes")); // image a partir du quadtree
		
		frame.add(interfaceDelta);
		frame.add(interfacePhi);
		frame.add(scrollImagesDelta);
		frame.add(scrollImagesPhi);
		frame.setSize(1000, 700);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		frame.setVisible(true);
	}
}
